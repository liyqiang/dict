// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: proto/dict_title.proto

package proto

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "github.com/golang/protobuf/ptypes/empty"
	wrappers "github.com/golang/protobuf/ptypes/wrappers"
	libresp "github.com/pku-hit/libresp"
	math "math"
)

import (
	context "context"
	api "github.com/micro/go-micro/v2/api"
	client "github.com/micro/go-micro/v2/client"
	server "github.com/micro/go-micro/v2/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Reference imports to suppress errors if they are not otherwise used.
var _ api.Endpoint
var _ context.Context
var _ client.Option
var _ server.Option

// Api Endpoints for Dict_Title service

func NewDict_TitleEndpoints() []*api.Endpoint {
	return []*api.Endpoint{}
}

// Client API for Dict_Title service

type Dict_TitleService interface {
	//添加职称字典（项）
	AddTitleItem(ctx context.Context, in *AddTitleDictRequest, opts ...client.CallOption) (*libresp.GenericResponse, error)
	//获取职称字典
	ListHollTitle(ctx context.Context, in *wrappers.StringValue, opts ...client.CallOption) (*libresp.ListResponse, error)
	//通过id获取单项职称
	GetTitleItemById(ctx context.Context, in *wrappers.StringValue, opts ...client.CallOption) (*libresp.GenericResponse, error)
	//通过职称id删除职称字典项，如果传入特定值，则清空字典表
	DeleteTitleItem(ctx context.Context, in *wrappers.StringValue, opts ...client.CallOption) (*libresp.Response, error)
}

type dict_TitleService struct {
	c    client.Client
	name string
}

func NewDict_TitleService(name string, c client.Client) Dict_TitleService {
	return &dict_TitleService{
		c:    c,
		name: name,
	}
}

func (c *dict_TitleService) AddTitleItem(ctx context.Context, in *AddTitleDictRequest, opts ...client.CallOption) (*libresp.GenericResponse, error) {
	req := c.c.NewRequest(c.name, "Dict_Title.AddTitleItem", in)
	out := new(libresp.GenericResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dict_TitleService) ListHollTitle(ctx context.Context, in *wrappers.StringValue, opts ...client.CallOption) (*libresp.ListResponse, error) {
	req := c.c.NewRequest(c.name, "Dict_Title.ListHollTitle", in)
	out := new(libresp.ListResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dict_TitleService) GetTitleItemById(ctx context.Context, in *wrappers.StringValue, opts ...client.CallOption) (*libresp.GenericResponse, error) {
	req := c.c.NewRequest(c.name, "Dict_Title.GetTitleItemById", in)
	out := new(libresp.GenericResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dict_TitleService) DeleteTitleItem(ctx context.Context, in *wrappers.StringValue, opts ...client.CallOption) (*libresp.Response, error) {
	req := c.c.NewRequest(c.name, "Dict_Title.DeleteTitleItem", in)
	out := new(libresp.Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Dict_Title service

type Dict_TitleHandler interface {
	//添加职称字典（项）
	AddTitleItem(context.Context, *AddTitleDictRequest, *libresp.GenericResponse) error
	//获取职称字典
	ListHollTitle(context.Context, *wrappers.StringValue, *libresp.ListResponse) error
	//通过id获取单项职称
	GetTitleItemById(context.Context, *wrappers.StringValue, *libresp.GenericResponse) error
	//通过职称id删除职称字典项，如果传入特定值，则清空字典表
	DeleteTitleItem(context.Context, *wrappers.StringValue, *libresp.Response) error
}

func RegisterDict_TitleHandler(s server.Server, hdlr Dict_TitleHandler, opts ...server.HandlerOption) error {
	type dict_Title interface {
		AddTitleItem(ctx context.Context, in *AddTitleDictRequest, out *libresp.GenericResponse) error
		ListHollTitle(ctx context.Context, in *wrappers.StringValue, out *libresp.ListResponse) error
		GetTitleItemById(ctx context.Context, in *wrappers.StringValue, out *libresp.GenericResponse) error
		DeleteTitleItem(ctx context.Context, in *wrappers.StringValue, out *libresp.Response) error
	}
	type Dict_Title struct {
		dict_Title
	}
	h := &dict_TitleHandler{hdlr}
	return s.Handle(s.NewHandler(&Dict_Title{h}, opts...))
}

type dict_TitleHandler struct {
	Dict_TitleHandler
}

func (h *dict_TitleHandler) AddTitleItem(ctx context.Context, in *AddTitleDictRequest, out *libresp.GenericResponse) error {
	return h.Dict_TitleHandler.AddTitleItem(ctx, in, out)
}

func (h *dict_TitleHandler) ListHollTitle(ctx context.Context, in *wrappers.StringValue, out *libresp.ListResponse) error {
	return h.Dict_TitleHandler.ListHollTitle(ctx, in, out)
}

func (h *dict_TitleHandler) GetTitleItemById(ctx context.Context, in *wrappers.StringValue, out *libresp.GenericResponse) error {
	return h.Dict_TitleHandler.GetTitleItemById(ctx, in, out)
}

func (h *dict_TitleHandler) DeleteTitleItem(ctx context.Context, in *wrappers.StringValue, out *libresp.Response) error {
	return h.Dict_TitleHandler.DeleteTitleItem(ctx, in, out)
}
