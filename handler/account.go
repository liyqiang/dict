package handler

import (
	"context"
	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/pku-hit/dict/proto"

	"github.com/micro/go-micro/util/log"
	"github.com/pku-hit/dict/component/database"
	"github.com/pku-hit/dict/model"

	"github.com/pku-hit/libresp"
)

type Acdict struct{}

func (e *Acdict) GetAccount(ctx context.Context, req * wrappers.StringValue,  resp * libresp.ListResponse) error {
	log.Log("Received Region.ListCountry request")
	dicts, err := database.GetAccount(req.Value)
	if err != nil {
		resp.GenerateListResponseWithInfo(libresp.GENERAL_ERROR, err.Error())
		return nil
	}
	resp.GenerateListResponseSucc(model.GetDictsAny(dicts))
	return nil
}
func (e *Acdict) DelAcDict(ctx context.Context, req *wrappers.StringValue, resp *libresp.Response) error {
	log.Log("Received Dict.DelDict request")
	err := database.DeleteDict(req.Value, true)
	if err != nil {
		resp.GenerateResponseWithInfo(libresp.GENERAL_ERROR, err.Error())
	} else {
		resp.GenerateResponseSucc()
	}
	return nil
}
func (e *Acdict) AddAccountDict(ctx context.Context, req *proto.AddAcDictRequest, resp *libresp.GenericResponse) error {
	log.Log("Received Dict.AddDict request")
	dict, err := database.NewAcDict(req.Category, req.ParentId, req.Code, req.Name, req.PyCode, req.Type, req.Value)
	if err != nil {
		resp.GenerateGenericResponseWithInfo(libresp.GENERAL_ERROR, err.Error())
		return nil
	}
	resp.GenerateGenericResponseSucc(model.GetDictAny(dict))
	return nil
}