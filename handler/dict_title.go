package handler

import (
	"context"
	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/micro/go-micro/util/log"
	"github.com/pku-hit/dict/component/database"
	"github.com/pku-hit/dict/model"
	"github.com/pku-hit/dict/proto"
	"github.com/pku-hit/libresp"
)

type DictTitle struct{}

func (e *DictTitle) ListHollTitle(ctx context.Context,str *wrappers.StringValue, resp *libresp.ListResponse) error{
	log.Log("ListHollTitle：")
	dicts, err := database.GetHollTitleDict()
	if err != nil{
		resp.GenerateListResponseWithInfo(libresp.GENERAL_ERROR,err.Error())
	} else{
		resp.GenerateListResponseSucc(model.GetDictsAny(dicts))
	}
	return nil
}

func (e *DictTitle) GetTitleItemById (ctx context.Context,str *wrappers.StringValue, resp *libresp.GenericResponse) error{
	log.Log("GetTitleItemById：")
	dict, err:=database.GetTitleDictByID(str.Value)
	if err != nil{
		resp.GenerateGenericResponseWithInfo(libresp.GENERAL_ERROR,err.Error())
	} else {
		resp.GenerateGenericResponseSucc(model.GetDictAny(dict))
	}
	return nil
}

func (e *DictTitle) AddTitleItem(ctx context.Context,req *proto.AddTitleDictRequest,resp *libresp.GenericResponse) error{
	log.Log("AddTitleItem：")
	dict, err:=database.NewTitleDict(req.Category,req.ParentId,req.Code,req.Name,req.PyCode, req.Type,req.Value)
	if err != nil{
		resp.GenerateGenericResponseWithInfo(libresp.GENERAL_ERROR,err.Error())
	} else {
		resp.GenerateGenericResponseSucc(model.GetDictAny(dict))
	}
	return nil
}

func (e *DictTitle) DeleteTitleItem(ctx context.Context,str *wrappers.StringValue, resp *libresp.Response) error  {
	log.Log("DeleteTitleItem：")
	err := database.DeleteTitleDict(str.Value, true)
	if err != nil {
		resp.GenerateResponseWithInfo(libresp.GENERAL_ERROR, err.Error())
	} else {
		resp.GenerateResponseSucc()
	}
	return nil
}
